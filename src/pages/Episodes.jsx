import React, { useEffect, useState } from 'react'
import NavBar from '../components/navbar/NavBar'
import Cards from '../components/cards/Cards'
import InputGroup from '../components/filters/category/InputGroup'

const Episodes = () => {
  const [id,setId]=useState(1)
  const [info,setInfo]=useState([])
  const[results,setResults]=useState([])

  let{air_date,name}=info  //destructuring

  let api=`https://rickandmortyapi.com/api/episode/${id}`

  useEffect(()=>{
    (async function (){
      let data=await fetch(api)
      .then((res)=>res.json());
      setInfo(data)

      let char=await Promise.all(
        data.characters.map((e)=>{
          return fetch(e).then((res)=>res.json())
        })
      )
      setResults(char)
    })();
  },[api])


  return (
    <div>
      <NavBar/>
      <div className="container">
        <div className="row mb-4">
          <h1 className="text-center my-4">Episode: <span className='text-primary'>{name===""?"Unknown":name}</span> </h1>
          <h5 className="text-center">Air Date : {air_date===""?"Unknown":air_date}</h5>

        </div>
        <div className="row">
          <div className="col-lg-3 col-12">
            <h4 className="text-center mb-4">Pick Episode</h4>
            <InputGroup setId={setId} name="Episode" total={51}/>
            

          </div>
          <div className="col-lg-8 col-12">
            <div className="row">
              <Cards page="/episodes/" results={results} />
            </div>
          </div>
          
        </div>
      </div>

      
    </div>
  )
}

export default Episodes