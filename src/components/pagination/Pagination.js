import React, { useState, useEffect } from "react";
import ReactPaginate from 'react-paginate';

const Pagination = ({info,setPageNumber,pageNumber}) => {

  const [width, setWidth] = useState(window.innerWidth);

  const updateDimensions = () => {
    setWidth(window.innerWidth);
  };

  useEffect(() => {
    window.addEventListener("resize", updateDimensions);
    return () => window.removeEventListener("resize", updateDimensions);
  }, []);

    

  return (
    <>

<style jsx>
        {`
          @media (max-width: 768px) {
            .pagination {
              font-size: 12px;
            }
            .next,
            .prev {
              display: none;
            }
          }
          @media (max-width: 768px) {
            .pagination {
              font-size: 14px;
            }
          }
        `}
      </style>


    <ReactPaginate
    
     className='pagination justify-content-center gap-4 my-2'
     forcePage={pageNumber===1? 0 : pageNumber-1}
     previousLinkClassName="text-decoration-none bg-primary text-light p-2 fw-bold rounded "
     nextLinkClassName="text-decoration-none bg-primary text-light p-2 fw-bold rounded "
     previousLabel="<<Prev"
     nextLabel="Next>>"
     pageclassName="page-item"
     pageLinkclassName="page-link "
     onPageChange={(data)=>{
      // console.log(data)
      setPageNumber(data.selected+1)
     }}
     activeLinkClassName="active"
    
     
     pageCount={info?.pages}
     
     /> 
     </>
     
      //info?--if info exists add .pages -since its comming from api..or else it goes when we refresh
  )
}

export default Pagination