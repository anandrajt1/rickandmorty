import React from 'react'
import { NavLink, Link } from "react-router-dom";
import "../../App.css";

const NavBar = () => {
  return (
    <div>
        <nav className="navbar navbar-expand-lg bg-body-tertiary">
  <div className="container">
  
  <Link to={'/'} className='fs-3 Ubuntu  navbar-brand'>Rick & Morty <span className="text-primary">Wiki</span> </Link>

  <style jsx>{`
          button[aria-expanded="false"] > .close {
            display: none;
          }
          button[aria-expanded="true"] > .open {
            display: none;
          }
        `}</style>

    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      
    <span class="fas fa-bars open text-dark"></span>
          <span class="fas fa-times close text-dark"></span>

    </button>
    <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
      <ul className="navbar-nav fs-5">
        <li className="nav-item">
          <NavLink  to={'/'} activeClassName="active"  className="nav-link " aria-current="page" >Characters</NavLink>
        </li>
        <li className="nav-item">
          <NavLink to={'/episodes'} activeClassName="active" className="nav-link" >Episodes</NavLink>
        </li>
        <li className="nav-item">
          <NavLink to={'/location'} activeClassName="active" className="nav-link" >Location</NavLink>
        </li>
        
      </ul>
    </div>
  </div>
</nav>
    </div>
  )
}

export default NavBar