import React from 'react'

const FilterBtn = ({name,index,items,task,setPageNumber,}) => {
  return (
    <>
    <style jsx>
        {`
        .form-check-input:checked+label{
            background-color:blue;
            color:white
        }


        input[type='radio']{
            display:none;
        }
         ` }


    </style>


    <div className="form-check">

  <input
  onClick={()=>{
    setPageNumber(1);
    task(items);
  }}
   className="form-check-input" 
   type="radio" 
   name={name}
   id={`${name}-${index}`} />

  <label class="btn btn-outline-primary" for={`${name}-${index}`}>
    {items}
  </label>

</div>

</>
  )
}

export default FilterBtn