import React from 'react'
import FilterBtn from '../FilterBtn'

const Gender = ({setPageNumber,setGender}) => {
    let gender=["Male","Female","Genderless","Unknown"]
  return (
    <div className="accordion-item">
    <h2 className="accordion-header">
      <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        Gender
      </button>
    </h2>
    <div id="collapseTwo" className="accordion-collapse collapse" data-bs-parent="#accordionExample">
      <div className="accordion-body d-flex flex-wrap gap-3">
        {
            gender.map((items,index)=>(
                <FilterBtn setPageNumber={setPageNumber} task={setGender} key={index} name="gender" items={items} index={index}/>
            ))
        }
      
      </div>
    </div>
  </div>
  )
}

export default Gender