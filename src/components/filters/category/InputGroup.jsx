import React from 'react'

const InputGroup = ({total,setId,name}) => {

    
  return (
    <div>
        <div class="input-group mb-3">
  <select onChange={(e)=>setId(e.target.value)} class="form-select" id="inputGroupSelect01">
    
    {
        [...Array(total).keys()].map(e=>{
            return <option value={e+1}>{name}-{e+1}</option>
        })

    }
    
   >
  </select>
</div>

    </div>
  )
}

export default InputGroup