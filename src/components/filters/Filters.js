import React from 'react'
import Status from './category/Status'
import Gender from './category/Gender'
import Species from './category/Species'


const Filters = ({setStatus, setPageNumber,setGender,setSpecies}) => {

  function clear(){
    setStatus("");
     setPageNumber("");
     setGender("");
     setSpecies("");
     window.location.reload(false); //to refresh the page

  }


  return (
    <div className="col-lg-3 col-12 mb-5">
      <div className="text-center fs-4 mb-2 fw-bold">Filters</div>

      <div onClick={clear} style={{cursor:"pointer"}} className="text-center text-decoration-underline text-primary mb-4">Clear Filters</div>

      <div className="accordion" id="accordionExample">

  <Status setPageNumber={setPageNumber} setStatus={setStatus} />

  <Gender setGender={setGender} setPageNumber={setPageNumber} />

  <Species setSpecies={setSpecies} setPageNumber={setPageNumber} />
  
 
</div>


      
  
  
  

    </div>
  )
}

export default Filters