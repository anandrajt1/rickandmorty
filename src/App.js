import React, { useState, useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap';
import Filters from './components/filters/Filters';
import Cards from './components/cards/Cards';
import Pagination from './components/pagination/Pagination';
import Search from './components/search/Search';
import NavBar from './components/navbar/NavBar';







function App() {

  // page number
  const [pageNumber, setPageNumber] = useState(1)

  const[search,setSearch]=useState("")

  const [fetchedData, updateFetchedData] = useState([])

  const[status,setStatus]=useState("")
  const[gender,setGender]=useState("")
  const[species,setSpecies]=useState("")

  // console.log(fetchedData.results)
  let { info, results } = fetchedData //array destructurng-passs info to pagnation,result to cards

  console.log(results)

  let api = `https://rickandmortyapi.com/api/character/?page=${pageNumber}&name=${search}&status=${status}&gender=${gender}&species=${species}`


  useEffect(() => {
    (async function () {
      let data = await fetch(api).then((res) => res.json());
      // console.log(data.results)
      updateFetchedData(data)

    })()
  }, [api])

  return (
    <div className="App">
      <NavBar/>

      <h1 className="text-center mt-2">Characters</h1>
      

      <Search setPageNumber={setPageNumber} setSearch={setSearch}/>

      <div className="container">

        <div className="row">

          
            <Filters setSpecies={setSpecies} setGender={setGender} setStatus={setStatus} setPageNumber={setPageNumber} />
         
          <div className="col-lg-8 col-12">
            <div className="row">
              <Cards page="/" results={results} />

              <Pagination info={info} pageNumber={pageNumber} setPageNumber={setPageNumber}/>

            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
