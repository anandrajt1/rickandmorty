import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import Episodes from './pages/Episodes';
import Location from './pages/Location';
import CardDetails from './components/cards/CardDetails';


const router = createBrowserRouter([
  {
    path: "/",
    element: <App/>,
  },
  {
    path: "/:id",
    element: <CardDetails/>,
  },
  {
    path: "/episodes",
    element: <Episodes/>,
  },
  {
    path: "/episodes/:id",
    element: <CardDetails/>,
  },
  {
    path: "/location",
    element: <Location/> ,
  },
  {
    path: "/location/:id",
    element: <CardDetails/>,
  }
]);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
